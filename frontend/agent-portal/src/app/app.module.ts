import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ApplicationFilterComponent } from './components/application-filter/application-filter.component';
import { ApplicationListComponent } from './components/application-list/application-list.component';
import { ApplicationHistoryComponent } from './pages/application-history/application-history.component';
import { NewApplicationComponent } from './pages/new-application/new-application.component';
import { CoursesInformationsComponent } from './components/courses-informations/courses-informations.component';
import { StudentInformationComponent } from './components/student-information/student-information.component';
import { DocumentsUploadComponent } from './components/documents-upload/documents-upload.component';
import { ProfileInformationComponent } from './pages/profile-information/profile-information.component';
import { ProfileFormComponent } from './components/profile-form/profile-form.component';
import { AgentDocumentsComponent } from './components/agent-documents/agent-documents.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { ApplicationDetailsComponent } from './pages/application-details/application-details.component';
import { ApplicationStatusComponent } from './components/application-status/application-status.component';
import { MessagesComponent } from './components/messages/messages.component';
import { StudentDocumentsComponent } from './components/student-documents/student-documents.component';
import { AgentOverviewComponent } from './components/agent-overview/agent-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    ApplicationFilterComponent,
    ApplicationListComponent,
    ApplicationHistoryComponent,
    NewApplicationComponent,
    CoursesInformationsComponent,
    StudentInformationComponent,
    DocumentsUploadComponent,
    ProfileInformationComponent,
    ProfileFormComponent,
    AgentDocumentsComponent,
    ChangePasswordComponent,
    ApplicationDetailsComponent,
    ApplicationStatusComponent,
    MessagesComponent,
    StudentDocumentsComponent,
    AgentOverviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
