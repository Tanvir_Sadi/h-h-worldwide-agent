import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationDetailsComponent } from './pages/application-details/application-details.component';
import { ApplicationHistoryComponent } from './pages/application-history/application-history.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { NewApplicationComponent } from './pages/new-application/new-application.component';
import { ProfileInformationComponent } from './pages/profile-information/profile-information.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'application-history', component: ApplicationHistoryComponent },
  { path: 'new-application', component: NewApplicationComponent },
  { path: 'profile', component: ProfileInformationComponent },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'application-details/:id', component: ApplicationDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
