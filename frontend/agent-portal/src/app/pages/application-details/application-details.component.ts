import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit {
  steps: any = 1;

  constructor() { }

  ngOnInit(): void {
  }
  step(step: any){
    this.steps = step;
  }
}
