import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesInformationsComponent } from './courses-informations.component';

describe('CoursesInformationsComponent', () => {
  let component: CoursesInformationsComponent;
  let fixture: ComponentFixture<CoursesInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursesInformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
